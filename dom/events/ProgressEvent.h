/* -*- Mode: C++; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set ts=8 sts=2 et sw=2 tw=80: */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef mozilla_dom_ProgressEvent_h
#define mozilla_dom_ProgressEvent_h

#include "mozilla/dom/Event.h"
#include "mozilla/dom/BindingUtils.h"
#include "nsCycleCollectionParticipant.h"
#include "nsIDOMProgressEvent.h"
#include "mozilla/dom/ProgressEventBinding.h"

namespace mozilla {
namespace dom {

struct ProgressEventInit;

/**
 * Implements the ProgressEvent event, used for measuring progress.
 *
 * See http://www.whatwg.org/specs/web-apps/current-work/#progress0 for
 * further details.
 */
class ProgressEvent : public Event,
                      public nsIDOMProgressEvent
{
public:
  ProgressEvent(EventTarget* aOwner);

  NS_DECL_ISUPPORTS_INHERITED
  NS_DECL_CYCLE_COLLECTION_SCRIPT_HOLDER_CLASS_INHERITED(ProgressEvent, Event)

  NS_DECL_NSIDOMPROGRESSEVENT

  // Forward to base class
  NS_FORWARD_TO_EVENT

  virtual JSObject* WrapObjectInternal(JSContext* aCx, JS::Handle<JSObject*> aGivenProto) override;

  virtual ProgressEvent* AsProgressEvent() override;

  static already_AddRefed<ProgressEvent> Constructor(EventTarget* aOwner, const nsAString& aType, const ProgressEventInit& aEventInitDict);

  static already_AddRefed<ProgressEvent> Constructor(const GlobalObject& aGlobal, const nsAString& aType, const ProgressEventInit& aEventInitDict, ErrorResult& aRv);

  bool LengthComputable() const;

  uint64_t Loaded() const;

  uint64_t Total() const;

protected:
  ~ProgressEvent();

private:
  bool mLengthComputable;
  uint64_t mLoaded;
  uint64_t mTotal;
};

} // namespace dom
} // namespace mozilla

#endif // mozilla_dom_ProgressEvent_h
