/* -*- Mode: IDL; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "nsIDOMHTMLElement.idl"

/**
 * The nsIDOMHTMLTableElement interface is the interface to a [X]HTML
 * table element.
 *
 * This interface is trying to follow the DOM Level 2 HTML specification:
 * http://www.w3.org/TR/DOM-Level-2-HTML/
 *
 * with changes from the work-in-progress WHATWG HTML specification:
 * http://www.whatwg.org/specs/web-apps/current-work/
 */

[uuid(135a30ee-0374-4ee7-9d36-91736bff5fb1)]
interface nsIDOMHTMLTableElement : nsISupports
{
  // Modified in DOM Level 2:
           attribute nsIDOMHTMLElement caption;
                                             // raises(DOMException) on setting

  // Modified in DOM Level 2:
           attribute nsIDOMHTMLElement tHead;
                                             // raises(DOMException) on setting

  // Modified in DOM Level 2:
           attribute nsIDOMHTMLElement tFoot;
                                             // raises(DOMException) on setting

  readonly attribute nsIDOMHTMLCollection          rows;
  readonly attribute nsIDOMHTMLCollection          tBodies;
           attribute DOMString                     align;
           attribute DOMString                     bgColor;
           attribute DOMString                     border;
           attribute DOMString                     cellPadding;
           attribute DOMString                     cellSpacing;
           attribute DOMString                     frame;
           attribute DOMString                     rules;
           attribute DOMString                     summary;
           attribute DOMString                     width;
  nsIDOMHTMLElement         createTHead();
  [binaryname(xpidlDeleteTHead)]
  void                      deleteTHead();
  nsIDOMHTMLElement         createTFoot();
  [binaryname(xpidlDeleteTFoot)]
  void                      deleteTFoot();
  nsIDOMHTMLElement         createCaption();
  [binaryname(xpidlDeleteCaption)]
  void                      deleteCaption();
  // Modified in DOM Level 2:
  nsIDOMHTMLElement         insertRow(in long index)
                                             raises(DOMException);
  // Modified in DOM Level 2:
  void                      deleteRow(in long index)
                                             raises(DOMException);
};
